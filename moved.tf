# To be remove after 1+

moved {
  from = data.aws_iam_policy_document.sts_ec2
  to   = data.aws_iam_policy_document.sts_ec2["0"]
}

moved {
  from = data.aws_iam_policy_document.sts_batch
  to   = data.aws_iam_policy_document.sts_batch["0"]
}

moved {
  from = data.aws_iam_policy_document.sts_spotfleet
  to   = data.aws_iam_policy_document.sts_spotfleet["0"]
}

moved {
  from = data.aws_subnet.this
  to   = data.aws_subnet.this["0"]
}

moved {
  from = aws_batch_compute_environment.this[0]
  to   = aws_batch_compute_environment.this["0"]
}

moved {
  from = aws_batch_job_queue.this[0]
  to   = aws_batch_job_queue.this["0"]
}

moved {
  from = aws_iam_role.ecs_instance_role[0]
  to   = aws_iam_role.ecs_instance_role["0"]
}

moved {
  from = aws_iam_role_policy_attachment.ecs_instance_role[0]
  to   = aws_iam_role_policy_attachment.ecs_instance_role["0"]
}

moved {
  from = aws_iam_instance_profile.ecs_instance_role[0]
  to   = aws_iam_instance_profile.ecs_instance_role["0"]
}

moved {
  from = aws_iam_role.service_role[0]
  to   = aws_iam_role.service_role["0"]
}

moved {
  from = aws_iam_role_policy_attachment.service_role[0]
  to   = aws_iam_role_policy_attachment.service_role["0"]
}

moved {
  from = aws_iam_role.service_role_spot[0]
  to   = aws_iam_role.service_role_spot["0"]
}

moved {
  from = aws_iam_role_policy_attachment.service_role_spot[0]
  to   = aws_iam_role_policy_attachment.service_role_spot["0"]
}

moved {
  from = aws_iam_service_linked_role.spotfleet[0]
  to   = aws_iam_service_linked_role.spotfleet["0"]
}

moved {
  from = aws_iam_service_linked_role.spot[0]
  to   = aws_iam_service_linked_role.spot["0"]
}

moved {
  from = aws_security_group.instances[0]
  to   = aws_security_group.instances["0"]
}

moved {
  from = aws_security_group_rule.instances_egress_open[0]
  to   = aws_security_group_rule.instances_egress_open["0"]
}

# To be remove after 1+
