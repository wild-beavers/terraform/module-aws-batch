#####
# Datasources
#####

data "aws_iam_policy_document" "sts_ec2" {
  for_each = var.ecs_instance_profile_create == true ? { 0 = 0 } : {}

  statement {
    sid     = "1"
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "ec2.amazonaws.com"
      ]
    }
  }
}

data "aws_iam_policy_document" "sts_batch" {
  for_each = var.service_role_create == true ? { 0 = 0 } : {}

  statement {
    sid     = "1"
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "batch.amazonaws.com"
      ]
    }
  }
}

data "aws_iam_policy_document" "sts_spotfleet" {
  for_each = var.service_role_spot_create == true ? { 0 = 0 } : {}

  statement {
    sid     = "1"
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "spotfleet.amazonaws.com"
      ]
    }
  }
}

data "aws_subnet" "this" {
  for_each = var.instance_sg_create == true ? { 0 = 0 } : {}

  id = var.compute_resource_subnet_ids[0]
}
