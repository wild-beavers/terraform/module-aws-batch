#####
# Batch
#####

output "batch_compute_environment_arn" {
  description = "The Amazon Resource Name (ARN) of the compute environment."
  value       = try(aws_batch_compute_environment.this["0"].arn, "")
}

output "batch_compute_environment_ecs_cluster_arn" {
  description = "The Amazon Resource Name (ARN) of the underlying Amazon ECS cluster used by the compute environment."
  value       = try(aws_batch_compute_environment.this["0"].ecs_cluster_arn, "")
}

output "batch_compute_environment_status" {
  description = "The current status of the compute environment (for example, CREATING or VALID)."
  value       = try(aws_batch_compute_environment.this["0"].status, "")
}

output "batch_compute_environment_status_reason" {
  description = "A short, human-readable string to provide additional details about the current status of the compute environment."
  value       = try(aws_batch_compute_environment.this["0"].status_reason, "")
}

output "batch_job_queue_this_arn" {
  description = "The Amazon Resource Name of the job queue."
  value       = try(aws_batch_job_queue.this["0"].arn, "")
}

#####
# Instance Profile
#####

output "iam_role_ecs_instance_role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = try(aws_iam_role.ecs_instance_role["0"].arn, "")
}

output "iam_role_ecs_instance_role_create_date" {
  description = "The creation date of the IAM role."
  value       = try(aws_iam_role.ecs_instance_role["0"].create_date, "")
}

output "iam_role_ecs_instance_role_description" {
  description = "The description of the role."
  value       = try(aws_iam_role.ecs_instance_role["0"].description, "")
}

output "iam_role_ecs_instance_role_id" {
  description = "The name of the role."
  value       = try(aws_iam_role.ecs_instance_role["0"].id, "")
}

output "iam_role_ecs_instance_role_name" {
  description = "The name of the role."
  value       = try(aws_iam_role.ecs_instance_role["0"].name, "")
}

output "iam_role_ecs_instance_role_unique_id" {
  description = "The stable and unique string identifying the role."
  value       = try(aws_iam_role.ecs_instance_role["0"].unique_id, "")
}


output "iam_instance_profile_ecs_instance_role_id" {
  description = "The instance profile's ID."
  value       = try(aws_iam_instance_profile.ecs_instance_role["0"].id, "")
}

output "iam_instance_profile_ecs_instance_role_arn" {
  description = "The ARN assigned by AWS to the instance profile."
  value       = try(aws_iam_instance_profile.ecs_instance_role["0"].arn, "")
}

output "iam_instance_profile_ecs_instance_role_create_date" {
  description = "The creation timestamp of the instance profile."
  value       = try(aws_iam_instance_profile.ecs_instance_role["0"].create_date, "")
}

output "iam_instance_profile_ecs_instance_role_name" {
  description = "The instance profile's name."
  value       = try(aws_iam_instance_profile.ecs_instance_role["0"].name, "")
}

output "iam_instance_profile_ecs_instance_role_path" {
  description = "The path of the instance profile in IAM."
  value       = try(aws_iam_instance_profile.ecs_instance_role["0"].path, "")
}

output "iam_instance_profile_ecs_instance_role_role" {
  description = "The role assigned to the instance profile."
  value       = try(aws_iam_instance_profile.ecs_instance_role["0"].role, "")
}

output "iam_instance_profile_ecs_instance_role_unique_id" {
  description = "The unique ID assigned by AWS."
  value       = try(aws_iam_instance_profile.ecs_instance_role["0"].unique_id, "")
}

#####
# Service Role EC2
#####

output "iam_role_service_role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = try(aws_iam_role.service_role["0"].arn, "")
}

output "iam_role_service_role_create_date" {
  description = "The creation date of the IAM role."
  value       = try(aws_iam_role.service_role["0"].create_date, "")
}

output "iam_role_service_role_description" {
  description = "The description of the role."
  value       = try(aws_iam_role.service_role["0"].description, "")
}

output "iam_role_service_role_id" {
  description = "The name of the role."
  value       = try(aws_iam_role.service_role["0"].id, "")
}

output "iam_role_service_role_name" {
  description = "The name of the role."
  value       = try(aws_iam_role.service_role["0"].name, "")
}

output "iam_role_service_role_unique_id" {
  description = "The stable and unique string identifying the role."
  value       = try(aws_iam_role.service_role["0"].unique_id, "")
}

#####
# Service Role Spot
#####

output "iam_role_service_role_spot_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = try(aws_iam_role.service_role_spot["0"].arn, "")
}

output "iam_role_service_role_spot_create_date" {
  description = "The creation date of the IAM role."
  value       = try(aws_iam_role.service_role_spot["0"].create_date, "")
}

output "iam_role_service_role_spot_description" {
  description = "The description of the role."
  value       = try(aws_iam_role.service_role_spot["0"].description, "")
}

output "iam_role_service_role_spot_id" {
  description = "The name of the role."
  value       = try(aws_iam_role.service_role_spot["0"].id, "")
}

output "iam_role_service_role_spot_name" {
  description = "The name of the role."
  value       = try(aws_iam_role.service_role_spot["0"].name, "")
}

output "iam_role_service_role_spot_unique_id" {
  description = "The stable and unique string identifying the role."
  value       = try(aws_iam_role.service_role_spot["0"].unique_id, "")
}

output "service_linked_role_spot_id" {
  description = "The Amazon Resource Name (ARN) of the role."
  value       = try(aws_iam_service_linked_role.spot["0"].id, "")
}

output "service_linked_role_spot_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = try(aws_iam_service_linked_role.spot["0"].arn, "")
}

output "service_linked_role_spot_name" {
  description = "The name of the role."
  value       = try(aws_iam_service_linked_role.spot["0"].name, "")
}

output "service_linked_role_spot_path" {
  description = "The path of the role."
  value       = try(aws_iam_service_linked_role.spot["0"].path, "")
}

output "service_linked_role_spot_unique_id" {
  description = "The stable and unique string identifying the role."
  value       = try(aws_iam_service_linked_role.spot["0"].unique_id, "")
}

output "service_linked_role_spotfleet_id" {
  description = "The Amazon Resource Name (ARN) of the role."
  value       = try(aws_iam_service_linked_role.spotfleet["0"].id, "")
}

output "service_linked_role_spotfleet_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = try(aws_iam_service_linked_role.spotfleet["0"].arn, "")
}

output "service_linked_role_spotfleet_name" {
  description = "The name of the role."
  value       = try(aws_iam_service_linked_role.spotfleet["0"].name, "")
}

output "service_linked_role_spotfleet_path" {
  description = "The path of the role."
  value       = try(aws_iam_service_linked_role.spotfleet["0"].path, "")
}

output "service_linked_role_spotfleet_unique_id" {
  description = "The stable and unique string identifying the role."
  value       = try(aws_iam_service_linked_role.spotfleet["0"].unique_id, "")
}

#####
# Security Group
#####

output "security_group_instances_id" {
  description = "The ID of the security group."
  value       = try(aws_security_group.instances["0"].id, "")
}

output "security_group_instances_arn" {
  description = "The ARN of the security group."
  value       = try(aws_security_group.instances["0"].arn, "")
}

output "security_group_instances_vpc_id" {
  description = "The VPC ID."
  value       = try(aws_security_group.instances["0"].vpc_id, "")
}

output "security_group_instances_owner_id" {
  description = "The owner ID."
  value       = try(aws_security_group.instances["0"].owner_id, "")
}

output "security_group_instances_name" {
  description = "The name of the security group."
  value       = try(aws_security_group.instances["0"].name, "")
}

output "security_group_instances_description" {
  description = "The description of the security group."
  value       = try(aws_security_group.instances["0"].description, "")
}

output "security_group_instances_ingress" {
  description = "The ingress rules."
  value       = try(aws_security_group.instances["0"].ingress, "")
}

output "security_group_instances_egress" {
  description = "The egress rules."
  value       = try(aws_security_group.instances["0"].egress, "")
}
